/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/GroundPoint.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear;

/**
 * An object at a specified point in the FlightGear airport.
 * 
 * @author $Author: gregh $
 * @version $Id: GroundPoint.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class GroundPoint extends GroundObject {

    private double x;
    private double y;

    /**
     * Creates a new GroundPoint.
     * @param x the point's X-ordinate.
     * @param y the point's Y-ordinate.
     */
    public GroundPoint(double x, double y) {
        setIdent(GroundObject.IDENT_UNKNOWN);
        setSelected(false);
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the point's X-ordinate, relative to the background image.
     * @return the point's X-ordinate.
     */
    public double getX() {
        return x;
    }

    /**
     * Sets the point's X-ordinate, relative to the background image.
     * @param x the point's X-ordinate.
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Gets the point's Y-ordinate, relative to the background image.
     * @return the point's Y-ordinate.
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the point's Y-ordinate, relative to the background image.
     * @param y the point's Y-ordinate.
     */
    public void setY(double y) {
        this.y = y;
    }

}

// ----- Revision History -----
// $Log: GroundPoint.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
