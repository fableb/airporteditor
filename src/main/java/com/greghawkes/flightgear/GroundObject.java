/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/GroundObject.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear;

/**
 * An object in a FlightGear airport.
 * 
 * @author $Author: gregh $
 * @version $Id: GroundObject.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public abstract class GroundObject {
    /**
     * Value of unknown or unset object identifier.
     */
    public static final int IDENT_UNKNOWN = -1;

    private int ident;
    private boolean selected;

    /**
     * Gets the object's identifier.
     * @return the object's identifier.
     */
    public int getIdent() {
        return ident;
    }

    /**
     * Sets the object's identifier.
     * @param ident the object's identifier.
     */
    public void setIdent(int ident) {
        this.ident = ident;
    }

    /**
     * Gets whether the object is currently selected.
     * @return True, if the object is selected; False, otherwise.
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets whether the object is selected.
     * @param selected set to True to select the object; False, to de-select it.
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

}

// ----- Revision History -----
// $Log: GroundObject.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
