/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/Editable.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.io.PrintWriter;
import javax.swing.JFrame;

/**
 * Declares that the object has properties that can be edited in a dialog box and saved to a file.
 * 
 * @author $Author: gregh $
 * @version $Id: Editable.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public interface Editable {

    /**
     * Display a dialog box containing the properties of the object.
     * 
     * @param parent the JFrame that is the parent of the dialog box.
     * @return True, if the display must be redrawn; False, otherwise.
     */
    boolean editProperties(JFrame parent);

    /**
     * Print the object's details to the specified PrintWriter.
     * 
     * @param pw the PrintWriter.
     */
    void println(PrintWriter pw);

}

// ----- Revision History -----
// $Log: Editable.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
