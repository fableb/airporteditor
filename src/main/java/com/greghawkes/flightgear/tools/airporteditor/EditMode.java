/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/EditMode.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

/**
 * The various editing modes used by the airport editing display panel.
 * 
 * @author $Author: gregh $
 * @version $Id: EditMode.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public enum EditMode {
    POINT,
    NODE,
    GATE,
    SEGMENT;
}

// ----- Revision History -----
// $Log: EditMode.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
