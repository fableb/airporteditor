/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/UIReferenceProperties.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import com.greghawkes.utils.CoordUtils;
import com.greghawkes.utils.StringUtils;
import javax.swing.JOptionPane;

/**
 * Reference point properties dialog box.
 * 
 * @author $Author: gregh $
 * @version $Id: UIReferenceProperties.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class UIReferenceProperties extends javax.swing.JDialog {

    private boolean isOkay;

    /** Creates new form UIAirportProperties */
    public UIReferenceProperties(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        isOkay = false;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCancel = new javax.swing.JButton();
        btnOkay = new javax.swing.JButton();
        pnlPoint = new javax.swing.JPanel();
        lbltLat = new javax.swing.JLabel();
        lblLon = new javax.swing.JLabel();
        txtLat = new javax.swing.JTextField();
        txtLon = new javax.swing.JTextField();
        lbltName = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setName("Form"); // NOI18N
        setResizable(false);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.greghawkes.flightgear.tools.airporteditor.AirportEditor.class).getContext().getResourceMap(UIReferenceProperties.class);
        btnCancel.setText(resourceMap.getString("btnCancel.text")); // NOI18N
        btnCancel.setName("btnCancel"); // NOI18N
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnOkay.setText(resourceMap.getString("btnOkay.text")); // NOI18N
        btnOkay.setName("btnOkay"); // NOI18N
        btnOkay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkayActionPerformed(evt);
            }
        });

        pnlPoint.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("pnlPoint.border.title"))); // NOI18N
        pnlPoint.setName("pnlPoint"); // NOI18N

        lbltLat.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbltLat.setText(resourceMap.getString("lbltLat.text")); // NOI18N
        lbltLat.setName("lbltLat"); // NOI18N

        lblLon.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLon.setText(resourceMap.getString("lblLon.text")); // NOI18N
        lblLon.setName("lblLon"); // NOI18N

        txtLat.setName("txtLat"); // NOI18N

        txtLon.setName("txtLon"); // NOI18N

        lbltName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbltName.setText(resourceMap.getString("lbltName.text")); // NOI18N
        lbltName.setName("lbltName"); // NOI18N

        txtName.setName("txtName"); // NOI18N

        javax.swing.GroupLayout pnlPointLayout = new javax.swing.GroupLayout(pnlPoint);
        pnlPoint.setLayout(pnlPointLayout);
        pnlPointLayout.setHorizontalGroup(
            pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPointLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPointLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbltName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))
                    .addGroup(pnlPointLayout.createSequentialGroup()
                        .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbltLat)
                            .addComponent(lblLon))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtLon, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
                            .addComponent(txtLat, javax.swing.GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE))))
                .addContainerGap())
        );

        pnlPointLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {lblLon, lbltLat, lbltName});

        pnlPointLayout.setVerticalGroup(
            pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPointLayout.createSequentialGroup()
                .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbltName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbltLat)
                    .addComponent(txtLat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPointLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblLon)
                    .addComponent(txtLon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnOkay)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancel, btnOkay});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlPoint, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOkay, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnCancel, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed

        isOkay = false;
        setVisible(false);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnOkayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkayActionPerformed

        StringBuffer buf = new StringBuffer();
        Double d = null;
        
        // Confirm that the latitude is valid
        try {
            d = CoordUtils.parse(txtLat.getText());
            if (!CoordUtils.isValidLatitude(d)) {
                buf.append("Latitude is out of range\n");
            }
        } catch (IllegalArgumentException iax) {
            buf.append(iax.getMessage()).append('\n');
        }

        // Confirm that the longitude is valid
        try {
            d = CoordUtils.parse(txtLon.getText());
            if (!CoordUtils.isValidLongitude(d)) {
                buf.append("Longitude is out of range\n");
            }
        } catch (IllegalArgumentException iax) {
            buf.append("Longitude is not valid\n");
        }
        if (buf.length() > 0) {
            JOptionPane.showMessageDialog(this, buf.toString(), "Invalid", JOptionPane.ERROR_MESSAGE);
            return;
        }

        isOkay = true;
        setVisible(false);
    }//GEN-LAST:event_btnOkayActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnOkay;
    private javax.swing.JLabel lblLon;
    private javax.swing.JLabel lbltLat;
    private javax.swing.JLabel lbltName;
    private javax.swing.JPanel pnlPoint;
    private javax.swing.JTextField txtLat;
    private javax.swing.JTextField txtLon;
    private javax.swing.JTextField txtName;
    // End of variables declaration//GEN-END:variables

    public boolean isAccepted() {
        return isOkay;
    }

    public Double getLat() {
        return CoordUtils.parse(txtLat.getText());
    }

    public void setLat(Double lat) {
        txtLat.setText((lat == null) ? StringUtils.EMPTY : CoordUtils.format(lat));
    }

    public Double getLon() {
        return CoordUtils.parse(txtLon.getText());
    }

    public void setLon(Double lon) {
        txtLon.setText((lon == null) ? StringUtils.EMPTY : CoordUtils.format(lon));
    }

    public void setRefName(String name) {
        txtName.setText(name);
    }
    
    public String getRefName() {
        return txtName.getText();
    }

}

// ----- Revision History -----
// $Log: UIReferenceProperties.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
