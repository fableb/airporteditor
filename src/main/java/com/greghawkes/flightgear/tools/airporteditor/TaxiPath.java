/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/TaxiPath.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.io.PrintWriter;
import javax.swing.JFrame;

import com.greghawkes.flightgear.GroundObject;

/**
 * A taxiway between two taxiway nodes.
 * 
 * @author $Author: gregh $
 * @version $Id: TaxiPath.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class TaxiPath extends GroundObject implements Editable, Drawable {

    public static final int FORWARD = 0x01;
    public static final int BACKWARD = 0x02;
    public static final int BOTH = (FORWARD | BACKWARD);

    private static final int ARROW_SIZE = 20;
    private static final GeneralPath ARROW_SYMBOL = new GeneralPath();
    static {
        ARROW_SYMBOL.moveTo(1.0, 0.5);
        ARROW_SYMBOL.lineTo(2.0, 0.0);
        ARROW_SYMBOL.lineTo(1.0, -0.5);
    }

    private TaxiNode start;
    private TaxiNode finish;
    private int direction;
    private boolean pushBackRoute;

    /**
     * Fetch the start TaxiNode for this TaxiPath.
     * @return the start TaxiNode for this TaxiPath.
     */
    public TaxiNode getStart() {
        return start;
    }

    /**
     * Set the start TaxiNode for this TaxiPath.
     * @param start the start TaxiNode for this TaxiPath.
     */
    public void setStart(TaxiNode start) {
        this.start = start;
    }

    /**
     * Fetch the finish TaxiNode for this TaxiPath.
     * @return the finish TaxiNode for this TaxiPath.
     */
    public TaxiNode getFinish() {
        return finish;
    }

    /**
     * Set the finish TaxiNode for this TaxiPath.
     * @param finish the finish TaxiNode for this TaxiPath.
     */
    public void setFinish(TaxiNode finish) {
        this.finish = finish;
    }

    /**
     * Fetch the directions in which this TaxiPath can be traversed.
     * @return bitwise-OR combination of TaxiPath.FORWARD and TaxiPath.BACKWARD.
     */
    public int getDirection() {
        return direction;
    }

    /**
     * Set the directions in which this TaxiPath can be traversed.
     * @param direction bitwise-OR combination of TaxiPath.FORWARD and TaxiPath.BACKWARD.
     */
    public void setDirection(int direction) {
        this.direction = direction;
    }

    /**
     * Fetch whether this TaxiPath is part of a pushback route from a terminal gate.
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @return True, if this TaxiPath is part of a pushback route from a terminal gate; False, otherwise.
     */
    public boolean isPushBackRoute() {
        return pushBackRoute;
    }

    /**
     * Sets whether this TaxiPath is part of a pushback route from a terminal gate.
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @param pushBackRoute True, if this TaxiPath is part of a pushback route from a terminal gate; False, otherwise.
     */
    public void setPushBackRoute(boolean pushBackRoute) {
        this.pushBackRoute = pushBackRoute;
    }

    public void println(PrintWriter pw) {
        pw.println("PATH\t" + getStart().getIdent() + "\t" + getFinish().getIdent() + "\t" + getDirection());
    }

    public void draw(Graphics2D g, AffineTransform xfmBackground, int imgWidth, int imgHeight, double scalingFactor) {
        g.setColor(getDrawingColour());
        double[] p = {
            getStart().getX() * imgWidth,
            getStart().getY() * imgHeight,
            getFinish().getX() * imgWidth,
            getFinish().getY() * imgHeight,
        };
        xfmBackground.transform(p, 0, p, 0, 2);
        int fx = (int) p[0];
        int fy = (int) p[1];
        int tx = (int) p[2];
        int ty = (int) p[3];
        g.drawLine(fx, fy, tx, ty);

        if (isForward()) {
            AffineTransform xfm = new AffineTransform(xfmBackground);
            xfm.translate(getStart().getX() * imgWidth, getStart().getY() * imgHeight);
            xfm.scale(ARROW_SIZE/scalingFactor/2.0, ARROW_SIZE/scalingFactor/2.0);
            double dx = p[0] - p[2];
            double dy = p[1] - p[3];
            xfm.rotate(-dx, -dy);

            GeneralPath t = new GeneralPath(ARROW_SYMBOL);
            t.transform(xfm);

            g.draw(t);
        }
        if (isBackward()) {
            AffineTransform xfm = new AffineTransform(xfmBackground);
            xfm.translate(getFinish().getX() * imgWidth, getFinish().getY() * imgHeight);
            xfm.scale(ARROW_SIZE/scalingFactor/2.0, ARROW_SIZE/scalingFactor/2.0);
            double dx = p[0] - p[2];
            double dy = p[1] - p[3];
            xfm.rotate(dx, dy);

            GeneralPath t = new GeneralPath(ARROW_SYMBOL);
            t.transform(xfm);

            g.draw(t);
        }
    }

    public Color getDrawingColour() {
        return (isSelected() ? Color.GREEN : Color.MAGENTA);
    }

    public boolean editProperties(JFrame parent) {
        UITaxiwayProperties pathBox = new UITaxiwayProperties(parent, true);
        pathBox.setLocationRelativeTo(parent);
        pathBox.setDirection(getDirection());
        pathBox.setVisible(true);

        if (pathBox.isAccepted()) {
            setDirection(pathBox.getDirection());
            return true;

        } else {
            return false;
        }
    }

    /**
     * Can this TaxiPath be traversed in the forward direction?
     * <p>Note that it is valid for a TaxiPath to be traversed in <em>both</em> directions.</p>
     * @return True, if the TaxiPath be traversed in the forward direction; False, otherwise.
     */
    private boolean isForward() {
        return ((direction & FORWARD) != 0);
    }

    /**
     * Can this TaxiPath be traversed in the reverse direction?
     * <p>Note that it is valid for a TaxiPath to be traversed in <em>both</em> directions.</p>
     * @return True, if the TaxiPath be traversed in the reverse direction; False, otherwise.
     */
    private boolean isBackward() {
        return ((direction & BACKWARD) != 0);
    }

}

// ----- Revision History -----
// $Log: TaxiPath.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
