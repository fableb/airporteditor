/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/EditPanel.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;

import com.greghawkes.flightgear.GroundPoint;
import com.greghawkes.flightgear.GroundObject;
import com.greghawkes.utils.StringUtils;

/**
 * The airport editing display panel.
 * 
 * @author $Author: gregh $
 * @version $Id: EditPanel.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class EditPanel extends javax.swing.JPanel {

    private static final long DBLCLICK_TIMEOUT = 300; // milliseconds
    private static final double SCALING_CHANGE = 1.0 / Math.sqrt(2.0);
    private static final AffineTransform UNIT_TRANSFORM = new AffineTransform();
    private static final int DEFAULT_WIDTH = 300;
    private static final int DEFAULT_HEIGHT = 200;

    private static final int CURSOR_IDLE = 0;
    private static final int CURSOR_PANNING = 1;
    private static final int CURSOR_DRAGNODE = 2;
    private static final int CURSOR_FROMEND = 3;
    private static final int CURSOR_TOEND = 4;

    private BufferedImage imgBackground;
    private int imgWidth;
    private int imgHeight;
    private AffineTransform xfmBackground;
    private String status;
    private double scalingFactor;
    private Airport airport;
    private EditMode editMode;
    private Object editObject;
    private GroundObject currentObject;
    private long currentTime;

    private int cursorMode = CURSOR_IDLE;
    private Point panFrom;

    /**
     * Constructs a new EditPanel.
     * @param airport the airport to appear in the panel.
     * @throws java.io.IOException
     */
    public EditPanel(Airport airport) throws IOException {
        super();
        setBackground(Color.WHITE);
        setName("myPanel");
        this.airport = airport;
        status = StringUtils.EMPTY;
        imgBackground = null;
        imgWidth = DEFAULT_WIDTH;
        imgHeight = DEFAULT_HEIGHT;
        setEditMode(EditMode.POINT, null);
        editObject = null;
        currentObject = null;
        currentTime = System.currentTimeMillis();
        loadBackground();

        addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    int mods = e.getModifiersEx();
                    if ((mods & InputEvent.CTRL_DOWN_MASK) != 0) {
                        // Ctrl key was down; start panning
                        cursorMode = CURSOR_PANNING;
                        panFrom = e.getLocationOnScreen();

                    } else if (editMode == EditMode.NODE) {
                        // No Ctrl key, and in Node mode; place a new node
                        placeNode(e.getX(), e.getY(), (TaxiNode) editObject);
                        setEditMode(EditMode.POINT, null);

                    } else if (editMode == EditMode.GATE) {
                        // No Ctrl key, and in Gate mode; place a new terminal gate
                        placeGate(e.getX(), e.getY(), (TerminalGate) editObject);
                        setEditMode(EditMode.POINT, null);

                    } else if (editMode == EditMode.SEGMENT) {
                        // No Ctrl key, and in segment mode; start placing a new taxiway segment
                        double vx = ((double) e.getX()) / imgWidth / scalingFactor;
                        double vy = ((double) e.getY()) / imgHeight / scalingFactor;
                        if (cursorMode == CURSOR_FROMEND) {
                            taxiFromClicked(vx, vy);
                        } else {
                            taxiToClicked(vx, vy);
                        }

                    } else if (editMode == EditMode.POINT) {
                        // No Ctrl key, and in Pointer mode; select a node
                        double vx = ((double) e.getX()) / imgWidth / scalingFactor;
                        double vy = ((double) e.getY()) / imgHeight / scalingFactor;
                        objectClicked(vx, vy);
                        if (currentObject instanceof GroundPoint) {
                            // Clicked on a node; may be about to drag
                            cursorMode = CURSOR_DRAGNODE;
                        }
                    }
                    repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (SwingUtilities.isLeftMouseButton(e)) {
                    if (cursorMode == CURSOR_TOEND) {
                        setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                    } else {
                        cursorMode = CURSOR_IDLE;
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    }
                    repaint();
                }
            }

        });

        addMouseMotionListener(new MouseAdapter() {
            
            @Override
            public void mouseDragged(MouseEvent e) {
                if (cursorMode == CURSOR_PANNING) {
                    Point panTo = e.getLocationOnScreen();
                    if (panFrom != null) {
                        int dx = (int) (panFrom.getX() - panTo.getX());
                        int dy = (int) (panFrom.getY() - panTo.getY());
                        if (dx != 0 || dy != 0) {
                            Rectangle vis = getVisibleRect();
                            int newx = (int) (vis.getX() + dx);
                            int newy = (int) (vis.getY() + dy);
                            vis.setLocation(newx, newy);
                            scrollRectToVisible(vis);
                            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                            repaint();
                        }
                    }
                    panFrom = panTo;


                } else if (cursorMode == CURSOR_DRAGNODE) {
                    if (currentObject instanceof GroundPoint) {
                        double vx = ((double) e.getX()) / imgWidth / scalingFactor;
                        double vy = ((double) e.getY()) / imgHeight / scalingFactor;
                        ((GroundPoint) currentObject).setX(vx);
                        ((GroundPoint) currentObject).setY(vy);
                        setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                        repaint();
                    }
                }
            }
        });

        addMouseWheelListener(new MouseWheelListener() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if ((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0) {
                    if (e.getWheelRotation() > 0) {
                        zoomOut(e.getX(), e.getY());
                    } else {
                        zoomIn(e.getX(), e.getY());
                    }
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            if (imgBackground != null) {
                g2.drawImage(imgBackground, xfmBackground, this);
            }

            for (TaxiNode n : airport.getNodes()) {
                n.draw(g2, xfmBackground, imgWidth, imgHeight, scalingFactor);
            }
            for (TaxiPath s : airport.getTaxiway()) {
                s.draw(g2, xfmBackground, imgWidth, imgHeight, scalingFactor);
            }

            airport.getRefPoint0().draw(g2, xfmBackground, imgWidth, imgHeight, scalingFactor);
            airport.getRefPoint1().draw(g2, xfmBackground, imgWidth, imgHeight, scalingFactor);

            g2.setColor(Color.BLACK);
            g2.drawString(status, 10, 20);
        }
    }

    public void loadBackground() throws IOException {
        String filename = airport.getImageFilename();

        imgBackground = ImageIO.read(new File(filename));
        imgWidth = imgBackground.getWidth();
        imgHeight = imgBackground.getHeight();
        zoomReset(imgWidth / 2, imgHeight / 2);
    }

    /**
     * Zoom in (display a smaller area of the airport).
     * @param x the X-Ordinate of the centre of the new display area.
     * @param y the Y-Ordinate of the centre of the new display area.
     */
    public void zoomIn(int x, int y) {
        Rectangle visible = getVisibleRect();

        scalingFactor /= SCALING_CHANGE;
        xfmBackground = new AffineTransform(scalingFactor, 0.0, 0.0, scalingFactor, 0.0, 0.0);
        int w = (int) (imgWidth * scalingFactor);
        int h = (int) (imgHeight * scalingFactor);
        Dimension size = new Dimension(w, h);
        setPreferredSize(size);
        setMaximumSize(size);
        revalidate();

        double vw = visible.getWidth();
        double vh = visible.getHeight();
        int tx = (int) (x / SCALING_CHANGE - vw/2.0);
        int ty = (int) (y / SCALING_CHANGE - vh/2.0);
        visible.setLocation(tx, ty);
        scrollRectToVisible(visible);

        repaint();
    }

    /**
     * Zoom in (display a larger area of the airport).
     * @param x the X-Ordinate of the centre of the new display area.
     * @param y the Y-Ordinate of the centre of the new display area.
     */
    public void zoomOut(int x, int y) {
        Rectangle visible = getVisibleRect();

        scalingFactor *= SCALING_CHANGE;
        xfmBackground = new AffineTransform(scalingFactor, 0.0, 0.0, scalingFactor, 0.0, 0.0);
        int w = (int) (imgWidth * scalingFactor);
        int h = (int) (imgHeight * scalingFactor);
        Dimension size = new Dimension(w, h);
        setPreferredSize(size);
        setMaximumSize(size);

        double vw = visible.getWidth();
        double vh = visible.getHeight();
        int tx = (int) (x * SCALING_CHANGE - vw/2.0);
        int ty = (int) (y * SCALING_CHANGE - vh/2.0);
        visible.setLocation(tx, ty);
        scrollRectToVisible(visible);

        revalidate();
        repaint();
    }

    /**
     * Reset the zoom level to the default.
     * @param x the X-Ordinate of the centre of the new display area.
     * @param y the Y-Ordinate of the centre of the new display area.
     */
    public void zoomReset(int x, int y) {
        Rectangle visible = getVisibleRect();

        scalingFactor = 1.0;
        xfmBackground = UNIT_TRANSFORM;
        Dimension size = new Dimension(imgWidth, imgHeight);
        setPreferredSize(size);
        setMaximumSize(size);
        revalidate();

        double vw = visible.getWidth();
        double vh = visible.getHeight();
        int tx = (int) (x - vw/2.0);
        int ty = (int) (y - vh/2.0);
        Rectangle newVisible = new Rectangle(tx, ty, (int) vw, (int) vh);
        scrollRectToVisible(newVisible);

        repaint();
    }

    /**
     * Fetch the airport displayed in the EditPanel.
     * @return the airport.
     */
    public Airport getAirport() {
        return airport;
    }

    public void setEditMode(EditMode m, Object obj) {
        editMode = m;
        editObject = obj;
        if (editMode == EditMode.NODE) {
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        } else if (editMode == EditMode.GATE) {
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        } else if (editMode == EditMode.SEGMENT) {
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            cursorMode = CURSOR_FROMEND;
        } else {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    /**
     * Delete the selects objects from the airport.
     */
    public void deleteSelected() {
        if (airport.deleteSelected()) {
            currentObject = null;
            repaint();
        }
    }

    /**
     * Place a new TaxiNode object in the airport.
     * @param x the X-ordinate of the new node.
     * @param y the Y-ordinate of the new node.
     * @param node the TaxiNode to be added to the airport.
     */
    private void placeNode(int x, int y, TaxiNode node) {
        double[] p = {
            ((double) x) / imgWidth,
            ((double) y) / imgHeight
        };
        try {
            xfmBackground.inverseTransform(p, 0, p, 0, 1);
            node.setX(p[0]);
            node.setY(p[1]);
            airport.addNode(node);
            repaint();

        } catch (NoninvertibleTransformException nix) {
            // Do nothing...
        }
    }

    /**
     * Place a new TerminalGate object in the airport.
     * @param x the X-ordinate of the new gate.
     * @param y the Y-ordinate of the new gate.
     * @param gate the TerminalGate to be added to the airport.
     */
    private void placeGate(int x, int y, TerminalGate gate) {
        double[] p = {
            ((double) x) / imgWidth,
            ((double) y) / imgHeight
        };
        try {
            xfmBackground.inverseTransform(p, 0, p, 0, 1);
            gate.setX(p[0]);
            gate.setY(p[1]);
            airport.addNode(gate);
            repaint();

        } catch (NoninvertibleTransformException nix) {
            // Do nothing...
        }
    }

    /**
     * Search the airport for the object near to the clicked location.
     * @param x the X-ordinate of the cursor location.
     * @param y the Y-ordinate of the cursor location.
     */
    private void objectClicked(double x, double y) {
        Point2D p = new Point2D.Double(x, y);
        GroundObject obj = airport.nearestPoint(p, scalingFactor);
        if (obj == null) {
            obj = airport.nearestLine(p, scalingFactor);
        }

        long now = System.currentTimeMillis();
        if (currentObject != null) {
            if (currentObject == obj) {
                // Clicked again on the selected object
                long elapsed = now - currentTime;
                if (elapsed < DBLCLICK_TIMEOUT) {
                    if (currentObject instanceof Editable) {
                        ((Editable) currentObject).editProperties(AirportEditor.getApplication().getMainFrame());
                    }
                }
                currentTime = now;
                return;
            }
            currentObject.setSelected(false);
        }
        currentObject = obj;
        if (currentObject != null) {
            currentObject.setSelected(true);
            currentTime = now;
        }
    }

    /**
     * Select the "From" node for a new TaxiPath.
     * <p>Saves the selected node for use when the user selects the "To" node.</p>
     * @param x the X-ordinate of the cursor location.
     * @param y the Y-ordinate of the cursor location.
     */
    public void taxiFromClicked(double x, double y) {
        Point2D p = new Point2D.Double(x, y);
        GroundPoint fromNode = airport.nearestPoint(p, scalingFactor);
        if (fromNode instanceof TaxiNode) {
            currentObject = fromNode;
            cursorMode = CURSOR_TOEND;
        }
    }

    /**
     * Select the "To" node for a new TaxiPath.
     * <p>Causes the new TaxiPath to be added to the airport.</p>
     * @param x the X-ordinate of the cursor location.
     * @param y the Y-ordinate of the cursor location.
     */
    public void taxiToClicked(double x, double y) {
        Point2D p = new Point2D.Double(x, y);
        GroundPoint toNode = airport.nearestPoint(p, scalingFactor);
        if (toNode instanceof TaxiNode) {
            TaxiPath segment = (TaxiPath) editObject;
            segment.setStart((TaxiNode) currentObject);
            segment.setFinish((TaxiNode) toNode);
            airport.addTaxiway((TaxiPath) editObject);

            setEditMode(EditMode.POINT, null);
            currentObject = null;
            cursorMode = CURSOR_IDLE;
            repaint();
        }
    }

}

// ----- Revision History -----
// $Log: EditPanel.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
