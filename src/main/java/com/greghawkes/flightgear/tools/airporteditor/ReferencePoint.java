/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/ReferencePoint.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.io.PrintWriter;
import javax.swing.JFrame;

import com.greghawkes.flightgear.GroundPoint;
import com.greghawkes.utils.StringUtils;

/**
 * Relates lat/lon coordinates to image coordinates.
 * 
 * @author $Author: gregh $
 * @version $Id: ReferencePoint.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class ReferencePoint extends GroundPoint implements Editable, Drawable {
    private static final int MARK_SIZE = 15;
    private static final GeneralPath MARK_SYMBOL = new GeneralPath();
    static {
        MARK_SYMBOL.moveTo(0.0, -1.0);
        MARK_SYMBOL.lineTo(0.0, 1.0);
        MARK_SYMBOL.moveTo(-1.0, 0.0);
        MARK_SYMBOL.lineTo(1.0, 0.0);
    }

    private String refName;
    private Double lat;
    private Double lon;

    /**
     * Construct a new ReferencePoint object.
     * @param x the X-ordinate on the airport's background image.
     * @param y the Y-ordinate on the airport's background image.
     */
    public ReferencePoint(double x, double y) {
        super(x, y);
        lat = null;
        lon = null;
    }

    /**
     * Gets the reference point's latitude.
     * @return the reference point's latitude.
     */
    public Double getLatitude() {
        return lat;
    }

    /**
     * Sets the reference point's latitude.
     * @param lat the reference point's latitude.
     */
    public void setLatitude(Double lat) {
        this.lat = lat;
    }

    /**
     * Gets the reference point's longitude.
     * @return the reference point's longitude.
     */
    public Double getLongitude() {
        return lon;
    }

    /**
     * Set the reference point's longitude.
     * @param lon the reference point's longitude.
     */
    public void setLongitude(Double lon) {
        this.lon = lon;
    }

    /**
     * Gets the name of the reference point.
     * @return the name of the reference point.
     */
    public String getRefName() {
        if (refName == null) {
            refName = StringUtils.EMPTY;
        }
        return refName;
    }

    /**
     * Sets the name of the reference point.
     * @param refName the name of the reference point.
     */
    public void setRefName(String refName) {
        this.refName = refName;
    }

    @Override
    public void draw(Graphics2D g, AffineTransform xfmBackground, int imgWidth, int imgHeight, double scalingFactor) {
        g.setColor(getDrawingColour());

        double tx = getX() * imgWidth;
        double ty = getY() * imgHeight;
        AffineTransform xfm = new AffineTransform(xfmBackground);
        xfm.translate(tx, ty);
        xfm.scale(MARK_SIZE/scalingFactor, MARK_SIZE/scalingFactor);
        GeneralPath p = new GeneralPath(MARK_SYMBOL);
        p.transform(xfm);
        g.draw(p);
        
        String name = getRefName();
        if (name != null && name.length() > 0) {
            g.drawString(name, (float) (tx * scalingFactor), (float) (ty * scalingFactor));
        }
    }

    @Override
    public void println(PrintWriter pw) {
        pw.println("REF\t" + getRefName() + "\t" + getX() + "\t" + getY() + "\t" + getLatitude() + "\t" + getLongitude());
    }

    public boolean editProperties(JFrame parent) {
        UIReferenceProperties refBox = new UIReferenceProperties(parent, true);
        refBox.setLocationRelativeTo(parent);
        refBox.setRefName(getRefName());
        refBox.setLat(getLatitude());
        refBox.setLon(getLongitude());
        refBox.setVisible(true);

        if (refBox.isAccepted()) {
            setRefName(refBox.getRefName());
            setLatitude(refBox.getLat());
            setLongitude(refBox.getLon());
            return true;

        } else {
            return false;
        }
    }

    public Color getDrawingColour() {
        return Color.BLACK;
    }

}

// ----- Revision History -----
// $Log: ReferencePoint.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
