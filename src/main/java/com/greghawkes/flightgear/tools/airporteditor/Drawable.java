/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/Drawable.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 * Declares that an objects is visible on the airport display.
 * 
 * @author $Author: gregh $
 * @version $Id: Drawable.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public interface Drawable {

    /**
     * Draw the object on the Graphics2D surface.
     * 
     * @param g the Graphics2D surface.
     * @param xfmBackground transforms the background image to the current scale.
     * @param imgWidth the width of the background image, in pixels.
     * @param imgHeight the height of the background image, in pixels.
     * @param zoomFactor the current zoom factor selected by the user.
     */
    void draw(Graphics2D g, AffineTransform xfmBackground, int imgWidth, int imgHeight, double zoomFactor);

    /**
     * Fetch the colour in which to draw this object.
     * @return the colour in which to draw this object.
     */
    Color getDrawingColour();

}

// ----- Revision History -----
// $Log: Drawable.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
