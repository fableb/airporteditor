/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/TaxiNode.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.PrintWriter;
import javax.swing.JFrame;

import com.greghawkes.flightgear.GroundPoint;
import com.greghawkes.utils.StringUtils;

/**
 * A point on the FlightGear airport's taxiway network.
 * 
 * @author $Author: gregh $
 * @version $Id: TaxiNode.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class TaxiNode extends GroundPoint implements Editable, Drawable {
    private static final String DEFAULT_HOLD_POINT_TYPE = "none";
    private static final int NODE_SIZE = 20;

    private String nodeName;
    private boolean onRunway;
    private String holdPointType;

    /**
     * Constructs a new TaxiNode.
     */
    public TaxiNode() {
        super(0.0, 0.0);
    }

    /**
     * Fetch the name of the node.
     * @return the name of the node.
     */
    public String getNodeName() {
        if (nodeName == null) {
            nodeName = StringUtils.EMPTY;
        }
        return nodeName;
    }

    /**
     * Sets the name of the node.
     * @param nodeName the name of the node.
     */
    public void setNodeName(String nodeName) {
        this.nodeName = StringUtils.trimToNull(nodeName);
    }

    /**
     * Whether this node is on a runway.
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @return True, if the node is on a runway; False, otherwise.
     */
    public boolean isOnRunway() {
        return onRunway;
    }

    /**
     * Sets whether this node is on a runway.
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @param onRunway set to True, if the node is on a runway; False, otherwise.
     */
    public void setOnRunway(boolean onRunway) {
        this.onRunway = onRunway;
    }

    /**
     * Fetch the type of "Hold Point".
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @return the type of "Hold Point".
     */
    public String getHoldPointType() {
        if (holdPointType == null) {
            holdPointType = DEFAULT_HOLD_POINT_TYPE;
        }
        return holdPointType;
    }

    /**
     * Set the type of "Hold Point".
     * <p>This attribute is not yet used by FlightGear. It is reserved for future expansion.</p>
     * @param holdPointType the type of "Hold Point".
     */
    public void setHoldPointType(String holdPointType) {
        this.holdPointType = StringUtils.trimToNull(holdPointType);
    }

    public void draw(Graphics2D g, AffineTransform xfmBackground, int imgWidth, int imgHeight, double scalingFactor) {
        g.setColor(getDrawingColour());
        double[] p = {
            getX() * imgWidth,
            getY() * imgHeight
        };
        xfmBackground.transform(p, 0, p, 0, 1);
        int tx = (int) p[0];
        int ty = (int) p[1];
        g.drawOval(tx - NODE_SIZE/2, ty - NODE_SIZE/2, NODE_SIZE, NODE_SIZE);
    }

    public void println(PrintWriter pw) {
        pw.println("NODE\t" + getIdent() + "\t" + getX() + "\t" + getY() + "\t" + getNodeName());
    }

    /**
     * Fetch the getDrawingColour in which to draw this object.
     * @return the getDrawingColour.
     */
    public Color getDrawingColour() {
        return (isSelected() ? Color.GREEN : Color.MAGENTA);
    }

    public boolean editProperties(JFrame parent) {
        UINodeProperties nodeBox = new UINodeProperties(parent, true);
        nodeBox.setLocationRelativeTo(parent);
        nodeBox.setNodeName(getNodeName());
        nodeBox.setVisible(true);

        if (nodeBox.isAccepted()) {
            setNodeName(nodeBox.getNodeName());
            return true;

        } else {
            return false;
        }
    }

}

// ----- Revision History -----
// $Log: TaxiNode.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
