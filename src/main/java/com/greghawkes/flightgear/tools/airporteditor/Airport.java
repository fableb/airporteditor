/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/Airport.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.greghawkes.utils.FileUtils;
import com.greghawkes.utils.StringUtils;
import com.greghawkes.utils.NumberUtils;
import com.greghawkes.flightgear.GroundPoint;
import com.greghawkes.utils.DateTimeUtils;

/**
 * Contains all of the information about a FlightGear airport.
 *
 * @author $Author: gregh $
 * @version $Id: Airport.java,v 1.2 2008-06-07 06:30:30 gregh Exp $
 */
public class Airport {
    private static final String FILE_IDENT = "FLIGHTGEAR_AIRPORT";
    private static final int FILE_VERSION = 1;

    private final ReferencePoint refPoint0 = new ReferencePoint(0.25, 0.25);
    private final ReferencePoint refPoint1 = new ReferencePoint(0.75, 0.75);
    private final List<TaxiNode> nodes = new ArrayList<TaxiNode>();
    private final List<TaxiPath> paths = new ArrayList<TaxiPath>();
    private String icaoCode;
    private String description;
    private String imageFilename;

    /**
     * Creates a new, empty Airport.
     */
    public Airport() {
    }

    /**
     * Gets the description of the airport.
     * @return the description of the airport.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the airport.
     * @param descr the description of the airport.
     */
    public void setDescription(String descr) {
        description = StringUtils.trimToNull(descr);
    }

    /**
     * Gets the name of the file containing the airport's background reference image.
     * @return the name of image file.
     */
    public String getImageFilename() {
        return imageFilename;
    }

    /**
     * Sets the name of the file containing the airport's background image.
     * @param imgFile the name of the image file.
     */
    public void setImageFilename(String imgFile) {
        imageFilename = StringUtils.trimToNull(imgFile);
    }

    /**
     * Adds a new TaxiNode to the airport.
     * <p>A TaxiNode includes TerminalGate nodes.</p>
     * @param node the TaxiNode to be added to the airport.
     */
    public void addNode(TaxiNode node) {
        nodes.add(node);
    }

    /**
     * Adds a new taxiway segment to the airport.
     * @param path the new taxiway segment.
     */
    public void addTaxiway(TaxiPath path) {
        TaxiNode start = path.getStart();
        TaxiNode finish = path.getFinish();
        if (start == null) {
            throw new IllegalArgumentException("Start node of path cannot be null");
        }
        if (finish == null) {
            throw new IllegalArgumentException("Finish node of path cannot be null");
        }
        if (start == finish) {
            throw new IllegalArgumentException("Start and finish node of path must be different");
        }

        for (TaxiPath s : paths) {
            if (start == s.getStart() && finish == s.getFinish()
                    || start == s.getFinish() && finish == s.getStart()) {
                // An existing line segment already connects the nodes; don't create another
                return;
            }
        }
        paths.add(path);
    }

    /**
     * Adds a new taxiwayt segment from the start to the finish nodes of the airport.
     * <p>The new taxiway segment goes one way (from start to finish), unless there
     * is an existing segment that already goes from finish to start.</p>
     *
     * @param start the start node.
     * @param finish the finish node.
     */
    public void addTaxiway(TaxiNode start, TaxiNode finish) {
        if (start == null) {
            throw new IllegalArgumentException("Start node of path cannot be null");
        }
        if (finish == null) {
            throw new IllegalArgumentException("Finish node of path cannot be null");
        }
        if (start == finish) {
            throw new IllegalArgumentException("Start and finish node of path must be different");
        }

        for (TaxiPath s : paths) {
            if (start == s.getStart() && finish == s.getFinish()) {
                // An existing line segment already connects the nodes in this direction; ensure the "forward" flag is set
                int dir = s.getDirection() | TaxiPath.FORWARD;
                s.setDirection(dir);
                return;

            } else if (start == s.getFinish() && finish == s.getStart()) {
                // An existing line segment already connects the nodes in the other direction; ensure the "backward" flag is set
                int dir = s.getDirection() | TaxiPath.BACKWARD;
                s.setDirection(dir);
                return;
            }
        }

        // Nodes are not connected; create a new path between them
        TaxiPath path = new TaxiPath();
        path.setStart(start);
        path.setFinish(finish);
        path.setDirection(TaxiPath.FORWARD);
        paths.add(path);
    }

    /**
     * Gets the list of GroundNodes in the airport.
     * @return a list containing the GroundNodes.
     */
    public List<TaxiNode> getNodes() {
        return nodes;
    }

    /**
     * Gets the list of taxiway segments in the airport.
     * @return a list containing the taxiway segments.
     */
    public List<TaxiPath> getTaxiway() {
        return paths;
    }

    /**
     * Gets the airport's first reference point.
     * @return the airport's first reference point.
     */
    public ReferencePoint getRefPoint0() {
        return refPoint0;
    }

    /**
     * Gets the airport's second reference point.
     * @return the airport's second reference point.
     */
    public ReferencePoint getRefPoint1() {
        return refPoint1;
    }

    /**
     * Gets the airport's ICAO code.
     * <p>Refer to the <a href="http://en.wikipedia.org/wiki/ICAO_airport_code">Wikipedia article</a> for more information.</p>
     * @return the airport's ICAO code.
     */
    public String getIcaoCode() {
        return icaoCode;
    }

    /**
     * Sets the airport's ICAO code.
     * <p>Refer to the <a href="http://en.wikipedia.org/wiki/ICAO_airport_code">Wikipedia article</a> for more information.</p>
     * @param icaoCode the airport's ICAO code.
     */
    public void setIcaoCode(String icaoCode) {
        this.icaoCode = StringUtils.trimToNull(icaoCode);
    }

    /**
     * Write the airport's details to a file.
     * @param pw the PrintWriter object to receive the airport's details.
     */
    public void write(PrintWriter pw) {
        pw.println(FILE_IDENT);
        pw.println(FILE_VERSION);
        pw.println("#");
        pw.println("# Created by: " + System.getProperty("user.name"));
        pw.println("# Last updated: " + DateTimeUtils.getDateTimeLabel(new Date()));
        pw.println();
        pw.println(getIcaoCode());
        pw.println(getDescription());
        pw.println(getImageFilename());

        pw.println(2);
        getRefPoint0().println(pw);
        getRefPoint1().println(pw);

        int i = 0;
        pw.println(nodes.size());
        for (TaxiNode node : nodes) {
            node.setIdent(i++);

            node.println(pw);
        }

        pw.println(paths.size());
        for (TaxiPath path : paths) {
            path.println(pw);
        }
    }

    /**
     * Read a new airport from a file.
     * @param br the BufferedReader from which to read the airport's details.
     * @return the new Airport object that was read from the file.
     * @throws java.io.IOException if the file cannot be read.
     */
    public static Airport read(BufferedReader br) throws IOException {
        String s = FileUtils.readLine(br);
        if (!FILE_IDENT.equals(s)) {
            throw new IOException("File is not a FlightGear Airport");
        }
        Integer version = NumberUtils.parseInteger(FileUtils.readLine(br));
        if (version == null) {
            throw new IOException("Cannot determine version of FlightGear Airport file");
        } else if (version != FILE_VERSION) {
            throw new IOException("File is wrong version of a FlightGear Airport");
        }

        Airport a = new Airport();
        a.setIcaoCode(FileUtils.readLine(br));
        a.setDescription(FileUtils.readLine(br));
        a.setImageFilename(FileUtils.readLine(br));

        // Skip line with number of reference points
        FileUtils.readLine(br);

        s = FileUtils.readLine(br);
        String[] fields = s.split("\t");
        a.getRefPoint0().setRefName(fields[1]);
        a.getRefPoint0().setX(NumberUtils.parseDouble(fields[2]));
        a.getRefPoint0().setY(NumberUtils.parseDouble(fields[3]));
        a.getRefPoint0().setLatitude(NumberUtils.parseDouble(fields[4]));
        a.getRefPoint0().setLongitude(NumberUtils.parseDouble(fields[5]));

        s = FileUtils.readLine(br);
        fields = s.split("\t");
        a.getRefPoint1().setRefName(fields[1]);
        a.getRefPoint1().setX(NumberUtils.parseDouble(fields[2]));
        a.getRefPoint1().setY(NumberUtils.parseDouble(fields[3]));
        a.getRefPoint1().setLatitude(NumberUtils.parseDouble(fields[4]));
        a.getRefPoint1().setLongitude(NumberUtils.parseDouble(fields[5]));

        int count = NumberUtils.parseInteger(FileUtils.readLine(br));
        for (int i = 0; i < count; i++) {
            s = FileUtils.readLine(br);
            fields = s.split("\t");

            if (fields[0].equals("GATE")) {
                TerminalGate node = new TerminalGate();
                node.setIdent(NumberUtils.parseInteger(fields[1]));
                node.setX(NumberUtils.parseDouble(fields[2]));
                node.setY(NumberUtils.parseDouble(fields[3]));
                node.setNodeName(fields[4]);
                node.setGateType(fields[5]);
                node.setAirlineCodes(fields[6]);
                node.setGateSize(NumberUtils.parseInteger(fields[7]));
                node.setHeading(NumberUtils.parseDouble(fields[8]));
                a.addNode(node);

            } else if (fields[0].equals("NODE")) {
                TaxiNode node = new TaxiNode();
                node.setIdent(NumberUtils.parseInteger(fields[1]));
                node.setX(NumberUtils.parseDouble(fields[2]));
                node.setY(NumberUtils.parseDouble(fields[3]));
                node.setNodeName(fields[4]);
                a.addNode(node);
            }
        }

        count = NumberUtils.parseInteger(FileUtils.readLine(br));
        for (int i = 0; i < count; i++) {
            s = FileUtils.readLine(br);
            fields = s.split("\t");

            if (fields[0].equals("PATH")) {
                int fromIdent = NumberUtils.parseInteger(fields[1]);
                int toIdent = NumberUtils.parseInteger(fields[2]);
                int dir = NumberUtils.parseInteger(fields[3]);

                TaxiPath path = new TaxiPath();
                path.setStart(a.findNode(fromIdent));
                path.setFinish(a.findNode(toIdent));
                path.setDirection(dir);
                a.addTaxiway(path);
            }
        }
        return a;
    }

    /**
     * Find the airport's point object nearest the specified coordinates.
     * @param p the coordinates near which to search.
     * @param scalingFactor determines the search radius (the "nearness").
     * @return the GroundPoint object that is near to the specified point, or Null if there is no such object.
     */
    public GroundPoint nearestPoint(Point2D p, double scalingFactor) {
        TaxiNode node = null;
        Point2D np = new Point2D.Double(0.0, 0.0);

        GroundPoint rp = getRefPoint0();
        np.setLocation(rp.getX(), rp.getY());
        double dist = np.distance(p);
        if (dist < 0.01 / scalingFactor) {
            return rp;
        }

        rp = getRefPoint1();
        np.setLocation(rp.getX(), rp.getY());
        dist = np.distance(p);
        if (dist < 0.01 / scalingFactor) {
            return rp;
        }

        dist = 0.0;
        for (TaxiNode n : nodes) {
            np.setLocation(n.getX(), n.getY());
            double d = np.distance(p);
            if (node == null) {
                node = n;
                dist = d;
            } else if (d < dist) {
                node = n;
                dist = d;
            }
        }
        return (dist < 0.01 / scalingFactor) ? node : null;
    }

    /**
     * Find the airport's line object nearest the specified coordinates.
     * @param p the coordinates near which to search.
     * @param scalingFactor determines the search radius (the "nearness").
     * @return the TaxiPath object that is near to the specified point, or Null if there is no such object.
     */
    public TaxiPath nearestLine(Point2D p, double scalingFactor) {
        TaxiPath line = null;
        double dist = 0.0;
        Line2D x = new Line2D.Double();
        for (TaxiPath n : paths) {
            x.setLine(n.getStart().getX(), n.getStart().getY(), n.getFinish().getX(), n.getFinish().getY());
            double d = x.ptSegDist(p);
            if (line == null) {
                line = n;
                dist = d;
            } else if (d < dist) {
                line = n;
                dist = d;
            }
        }
        return (dist < 0.01 / scalingFactor) ? line : null;
    }

    /**
     * Find the airport's node with the specified identifier.
     * @param ident the identifier of the node.
     * @return the node with the specified identifier, or null if no such node exists.
     */
    public TaxiNode findNode(int ident) {
        for (TaxiNode n : nodes) {
            if (n.getIdent() == ident) {
                return n;
            }
        }
        return null;
    }

    /**
     * Delete the selected objects from the airport.
     * @return True if the view must be redrawn; False, otherwise.
     */
    public boolean deleteSelected() {
        int count = 0;
        for (Iterator<TaxiPath> i = paths.iterator(); i.hasNext(); ) {
            TaxiPath s = i.next();

            if (s.isSelected()) {
                i.remove();
                count++;
            }
        }
        for (Iterator<TaxiNode> i = nodes.iterator(); i.hasNext(); ) {
            TaxiNode n = i.next();

            if (n.isSelected()) {
                int used = 0;
                for (TaxiPath s : paths) {
                    if (s.getStart() == n || s.getFinish() == n) {
                        used++;
                    }
                }
                if (used == 0) {
                    // Node is not in use, and can be deleted
                    i.remove();
                }
            }
        }
        return (count > 0);
    }

}

// ----- Revision History -----
// $Log: Airport.java,v $
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
