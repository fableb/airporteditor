/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/UIAirportEditor.java,v 1.3 2008-06-12 14:05:46 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.event.InternalFrameListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;

import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.TaskMonitor;
import org.xml.sax.SAXException;

import com.greghawkes.utils.FileUtils;
import com.greghawkes.utils.LinearInterpolator;
import com.greghawkes.utils.StringUtils;
import javax.swing.event.InternalFrameEvent;

/**
 * The FlightGear airport editor's main frame.
 * 
 * @author $Author: gregh $
 * @version $Id: UIAirportEditor.java,v 1.3 2008-06-12 14:05:46 gregh Exp $
 */
public class UIAirportEditor extends FrameView {

    private static final String PROP_PANEL = "network.panel";
    private static final String PROP_FILENAME = "filename";

    private static final FileFilter FILTER_FGAP = new FileNameExtensionFilter("Airport files (*.fgap)", new String[] { "fgap" });
    private static final FileFilter FILTER_XML = new FileNameExtensionFilter("XML files (*.xml)", new String[] { "xml" });

    public UIAirportEditor(SingleFrameApplication app) {
        super(app);

        initComponents();

        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                statusMessageLabel.setText(StringUtils.EMPTY);
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? StringUtils.EMPTY : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
    }

    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            JFrame mainFrame = AirportEditor.getApplication().getMainFrame();
            aboutBox = new UIAbout(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        AirportEditor.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        jdpDesktop = new javax.swing.JDesktopPane();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        closeMenuItem = new javax.swing.JMenuItem();
        propertyMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        importMenu = new javax.swing.JMenu();
        importMenuItem = new javax.swing.JMenuItem();
        exportMenu = new javax.swing.JMenu();
        exportMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        insertMenu = new javax.swing.JMenu();
        nodeMenuItem = new javax.swing.JMenuItem();
        gateMenuItem = new javax.swing.JMenuItem();
        taxiwayMenuItem = new javax.swing.JMenuItem();
        objectPropMenuItem = new javax.swing.JMenuItem();
        deleteMenuItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        zoomInMenuItem = new javax.swing.JMenuItem();
        zoomOutMenuItem = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JSeparator();
        zoomResetMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusMessageLabel = new javax.swing.JLabel();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setLayout(new java.awt.BorderLayout());

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.greghawkes.flightgear.tools.airporteditor.AirportEditor.class).getContext().getResourceMap(UIAirportEditor.class);
        jdpDesktop.setBackground(resourceMap.getColor("jdpDesktop.background")); // NOI18N
        jdpDesktop.setName("jdpDesktop"); // NOI18N
        mainPanel.add(jdpDesktop, java.awt.BorderLayout.CENTER);

        menuBar.setName("menuBar"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.greghawkes.flightgear.tools.airporteditor.AirportEditor.class).getContext().getActionMap(UIAirportEditor.class, this);
        fileMenu.setAction(actionMap.get("showProperties")); // NOI18N
        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        newMenuItem.setAction(actionMap.get("createAirport")); // NOI18N
        newMenuItem.setText(resourceMap.getString("newMenuItem.text")); // NOI18N
        newMenuItem.setName("newMenuItem"); // NOI18N
        fileMenu.add(newMenuItem);

        openMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openMenuItem.setMnemonic('O');
        openMenuItem.setText(resourceMap.getString("openMenuItem.text")); // NOI18N
        openMenuItem.setName("openMenuItem"); // NOI18N
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        closeMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        closeMenuItem.setMnemonic('C');
        closeMenuItem.setText(resourceMap.getString("closeMenuItem.text")); // NOI18N
        closeMenuItem.setEnabled(false);
        closeMenuItem.setName("closeMenuItem"); // NOI18N
        fileMenu.add(closeMenuItem);

        propertyMenuItem.setAction(actionMap.get("showProperties")); // NOI18N
        propertyMenuItem.setName("propertyMenuItem"); // NOI18N
        fileMenu.add(propertyMenuItem);

        jSeparator1.setName("jSeparator1"); // NOI18N
        fileMenu.add(jSeparator1);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setMnemonic('S');
        saveMenuItem.setText(resourceMap.getString("saveMenuItem.text")); // NOI18N
        saveMenuItem.setEnabled(false);
        saveMenuItem.setName("saveMenuItem"); // NOI18N
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText(resourceMap.getString("saveAsMenuItem.text")); // NOI18N
        saveAsMenuItem.setEnabled(false);
        saveAsMenuItem.setName("saveAsMenuItem"); // NOI18N
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        importMenu.setText(resourceMap.getString("importMenu.text")); // NOI18N
        importMenu.setToolTipText(resourceMap.getString("importMenu.toolTipText")); // NOI18N
        importMenu.setEnabled(false);
        importMenu.setName("importMenu"); // NOI18N

        importMenuItem.setAction(actionMap.get("importParking")); // NOI18N
        importMenuItem.setToolTipText(resourceMap.getString("importMenuItem.toolTipText")); // NOI18N
        importMenuItem.setName("importMenuItem"); // NOI18N
        importMenu.add(importMenuItem);

        fileMenu.add(importMenu);

        exportMenu.setText(resourceMap.getString("exportMenu.text")); // NOI18N
        exportMenu.setEnabled(false);
        exportMenu.setName("exportMenu"); // NOI18N

        exportMenuItem.setAction(actionMap.get("exportParking")); // NOI18N
        exportMenuItem.setName("exportMenuItem"); // NOI18N
        exportMenu.add(exportMenuItem);

        fileMenu.add(exportMenu);

        jSeparator2.setName("jSeparator2"); // NOI18N
        fileMenu.add(jSeparator2);

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setText(resourceMap.getString("exitMenuItem.text")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setMnemonic('E');
        editMenu.setText(resourceMap.getString("editMenu.text")); // NOI18N
        editMenu.setEnabled(false);
        editMenu.setName("editMenu"); // NOI18N

        insertMenu.setText(resourceMap.getString("insertMenu.text")); // NOI18N
        insertMenu.setName("insertMenu"); // NOI18N

        nodeMenuItem.setAction(actionMap.get("createNode")); // NOI18N
        nodeMenuItem.setText(resourceMap.getString("nodeMenuItem.text")); // NOI18N
        nodeMenuItem.setName("nodeMenuItem"); // NOI18N
        insertMenu.add(nodeMenuItem);

        gateMenuItem.setAction(actionMap.get("createGate")); // NOI18N
        gateMenuItem.setText(resourceMap.getString("gateMenuItem.text")); // NOI18N
        gateMenuItem.setName("gateMenuItem"); // NOI18N
        insertMenu.add(gateMenuItem);

        taxiwayMenuItem.setAction(actionMap.get("createTaxiway")); // NOI18N
        taxiwayMenuItem.setName("taxiwayMenuItem"); // NOI18N
        insertMenu.add(taxiwayMenuItem);

        editMenu.add(insertMenu);

        objectPropMenuItem.setAction(actionMap.get("editProperties")); // NOI18N
        objectPropMenuItem.setText(resourceMap.getString("objectPropMenuItem.text")); // NOI18N
        objectPropMenuItem.setName("objectPropMenuItem"); // NOI18N
        editMenu.add(objectPropMenuItem);

        deleteMenuItem.setAction(actionMap.get("deleteSelected")); // NOI18N
        deleteMenuItem.setText(resourceMap.getString("deleteMenuItem.text")); // NOI18N
        deleteMenuItem.setName("deleteMenuItem"); // NOI18N
        editMenu.add(deleteMenuItem);

        menuBar.add(editMenu);

        viewMenu.setMnemonic('V');
        viewMenu.setText(resourceMap.getString("viewMenu.text")); // NOI18N
        viewMenu.setEnabled(false);
        viewMenu.setName("viewMenu"); // NOI18N

        zoomInMenuItem.setAction(actionMap.get("zoomIn")); // NOI18N
        zoomInMenuItem.setText(resourceMap.getString("zoomInMenuItem.text")); // NOI18N
        zoomInMenuItem.setName("zoomInMenuItem"); // NOI18N
        viewMenu.add(zoomInMenuItem);

        zoomOutMenuItem.setAction(actionMap.get("zoomOut")); // NOI18N
        zoomOutMenuItem.setText(resourceMap.getString("zoomOutMenuItem.text")); // NOI18N
        zoomOutMenuItem.setName("zoomOutMenuItem"); // NOI18N
        viewMenu.add(zoomOutMenuItem);

        jSeparator4.setName("jSeparator4"); // NOI18N
        viewMenu.add(jSeparator4);

        zoomResetMenuItem.setAction(actionMap.get("zoomReset")); // NOI18N
        zoomResetMenuItem.setText(resourceMap.getString("zoomResetMenuItem.text")); // NOI18N
        zoomResetMenuItem.setName("zoomResetMenuItem"); // NOI18N
        viewMenu.add(zoomResetMenuItem);

        menuBar.add(viewMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(statusMessageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 246, Short.MAX_VALUE)
                .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusAnimationLabel)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
    JInternalFrame fr = jdpDesktop.getSelectedFrame();//GEN-LAST:event_saveMenuItemActionPerformed
        if (fr == null) {
            return;
        }

        String filename = (String) fr.getClientProperty(PROP_FILENAME);
        EditPanel p = (EditPanel) fr.getClientProperty(PROP_PANEL);
        if (p == null) {
            return;
        }

        Airport a = p.getAirport();

        if (filename == null) {
            saveAsMenuItemActionPerformed(evt);

        } else {
            String msg = null;
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(filename);
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(fos));
                a.write(pw);
                FileUtils.closeQuietly(pw);

            } catch (IOException iox) {
                msg = "Can't save file '" + filename + "'\n" + iox.getClass().getName() + ": " + iox.getMessage();

            } finally {
                FileUtils.closeQuietly(fos);
            }
            if (msg != null) {
                JOptionPane.showMessageDialog(getFrame(), msg, "Can't Save file", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
    JFileChooser fc = new JFileChooser();
    fc.setDialogTitle("Select file to open");
    fc.setFileFilter(FILTER_FGAP);
    int result = fc.showOpenDialog(getFrame());
    if (result != JFileChooser.APPROVE_OPTION) {
        return;
    }

    File f = fc.getSelectedFile();
    if (f == null) {
        return;
    }

    String filename = f.getAbsolutePath();
    if (!f.isFile()) {
        JOptionPane.showMessageDialog(getFrame(), filename + " is not a file");
        return;
    }
    if (!f.canRead()) {
        JOptionPane.showMessageDialog(getFrame(), filename + " is not readable");
        return;
    }

    String msg = null;
    FileInputStream fis = null;
    Airport airport = null;
    try {
        fis = new FileInputStream(f);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        airport = Airport.read(br);
        FileUtils.closeQuietly(br);

    } catch (IOException ex) {
        msg = "Can't read file named '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();

    } finally {
        FileUtils.closeQuietly(fis);
    }
    if (msg != null) {
        JOptionPane.showMessageDialog(this.getFrame(), msg);
        return;
    }

    openEditWindow(airport, filename);
}//GEN-LAST:event_openMenuItemActionPerformed

private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
    JInternalFrame fr = jdpDesktop.getSelectedFrame();
    if (fr == null) {
        return;
    }

    EditPanel p = (EditPanel) fr.getClientProperty(PROP_PANEL);
    if (p == null) {
        return;
    }

    Airport a = p.getAirport();

    String filename = (String) fr.getClientProperty(PROP_FILENAME);
    JFileChooser fc = new JFileChooser();
    File f = null;
    if (filename != null) {
        f = new File(filename);
        fc.setSelectedFile(f);
    }
    fc.setDialogTitle("Select file to save");
    fc.setFileFilter(FILTER_FGAP);
    int result = fc.showSaveDialog(getFrame());
    if (result != JFileChooser.APPROVE_OPTION) {
        return;
    }

    f = fc.getSelectedFile();
    filename = FileUtils.setExtension(f.getAbsolutePath(), "fgap");

    f = new File(filename);
    if (f.exists()) {
        if (!f.canWrite()) {
            JOptionPane.showMessageDialog(this.getFrame(), filename + " is not writeable");
            return;
        }
        int option = JOptionPane.showConfirmDialog(this.getFrame(), "Are you sure you want to overwrite " + filename + "?", "Confirm", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (option != JOptionPane.OK_OPTION) {
            return;
        }
    }
    fr.putClientProperty(PROP_FILENAME, filename);

    String msg = null;
    FileOutputStream fos = null;
    try {
        fos = new FileOutputStream(f);
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(fos));
        a.write(pw);
        FileUtils.closeQuietly(pw);

    } catch (IOException ex) {
        msg = "Can't save as file named '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();

    } finally {
        FileUtils.closeQuietly(fos);
    }
    if (msg != null) {
        JOptionPane.showMessageDialog(getFrame(), msg, "Can't Save As file", JOptionPane.ERROR_MESSAGE);
    }
}//GEN-LAST:event_saveAsMenuItemActionPerformed

    @Action
    public void createAirport() {
        UIAirportProperties ap = new UIAirportProperties(getFrame(), true);
        ap.setLocationRelativeTo(getFrame());
        ap.setVisible(true);
        if (!ap.isAccepted()) {
            return;
        }

        Airport airport = new Airport();
        airport.setDescription(ap.getDescription());
        airport.setImageFilename(ap.getImageFilename());

        openEditWindow(airport, null);
    }

    @Action
    public void zoomIn() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                panel.zoomIn(0, 0);
            }
        }
    }

    @Action
    public void zoomOut() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                panel.zoomOut(0, 0);
            }
        }
    }

    @Action
    public void zoomReset() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                panel.zoomReset(0, 0);
            }
        }
    }

    @Action
    public void showProperties() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr == null) {
            return;
        }
        EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
        if (panel == null) {
            return;
        }
        Airport airport = panel.getAirport();

        UIAirportProperties propertyBox = new UIAirportProperties(getFrame(), true);
        propertyBox.setLocationRelativeTo(getFrame());
        propertyBox.setIcaoCode(airport.getIcaoCode());
        propertyBox.setDescription(airport.getDescription());
        propertyBox.setImageFilename(airport.getImageFilename());
        propertyBox.setVisible(true);

        if (propertyBox.isAccepted()) {
            airport.setIcaoCode(propertyBox.getIcaoCode());
            airport.setDescription(propertyBox.getDescription());
            airport.setImageFilename(propertyBox.getImageFilename());

            fr.setTitle(airport.getDescription());
            try {
                panel.loadBackground();
            } catch (IOException ex) {
                // Do nothing...
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem closeMenuItem;
    private javax.swing.JMenuItem deleteMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JMenu exportMenu;
    private javax.swing.JMenuItem exportMenuItem;
    private javax.swing.JMenuItem gateMenuItem;
    private javax.swing.JMenu importMenu;
    private javax.swing.JMenuItem importMenuItem;
    private javax.swing.JMenu insertMenu;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JDesktopPane jdpDesktop;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JMenuItem nodeMenuItem;
    private javax.swing.JMenuItem objectPropMenuItem;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JMenuItem propertyMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JMenuItem taxiwayMenuItem;
    private javax.swing.JMenu viewMenu;
    private javax.swing.JMenuItem zoomInMenuItem;
    private javax.swing.JMenuItem zoomOutMenuItem;
    private javax.swing.JMenuItem zoomResetMenuItem;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private UIAbout aboutBox;

    /**
     *
     * @param airport
     * @param filename
     */
    private void openEditWindow(Airport airport, String filename) {
        EditPanel jPanel = null;
        try {
            jPanel = new EditPanel(airport);

        } catch (IOException iox) {
            JOptionPane.showMessageDialog(this.getFrame(), iox.getMessage(), "Can't create airport", JOptionPane.ERROR_MESSAGE);
            return;

        } catch (OutOfMemoryError omx) {
            JOptionPane.showMessageDialog(this.getFrame(), "Out of memory: " + omx.getMessage(), "Can't create airport", JOptionPane.ERROR_MESSAGE);
            return;
        }

        javax.swing.JInternalFrame networkFrame = new javax.swing.JInternalFrame(airport.getDescription(), true, true, true, true);
        networkFrame.setSize(500, 400);
        networkFrame.setMinimumSize(new Dimension(300, 200));
        if (filename != null) {
            networkFrame.putClientProperty(PROP_FILENAME, filename);
        }
        networkFrame.addInternalFrameListener(new InternalFrameListener() {

            public void internalFrameOpened(InternalFrameEvent e) {
//                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void internalFrameClosing(InternalFrameEvent e) {
//                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void internalFrameClosed(InternalFrameEvent e) {
//                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void internalFrameIconified(InternalFrameEvent e) {
//                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void internalFrameDeiconified(InternalFrameEvent e) {
//                throw new UnsupportedOperationException("Not supported yet.");
            }

            public void internalFrameActivated(InternalFrameEvent e) {
                editMenu.setEnabled(true);
                viewMenu.setEnabled(true);
                closeMenuItem.setEnabled(true);
                propertyMenuItem.setEnabled(true);
                saveMenuItem.setEnabled(true);
                saveAsMenuItem.setEnabled(true);
                exportMenu.setEnabled(true);
                importMenu.setEnabled(true);
            }

            public void internalFrameDeactivated(InternalFrameEvent e) {
                editMenu.setEnabled(false);
                viewMenu.setEnabled(false);
                closeMenuItem.setEnabled(false);
                propertyMenuItem.setEnabled(false);
                saveMenuItem.setEnabled(false);
                saveAsMenuItem.setEnabled(false);
                exportMenu.setEnabled(false);
                importMenu.setEnabled(false);
            }

        });

        javax.swing.JScrollPane jScrollPane = new javax.swing.JScrollPane();
        jScrollPane.setName("jScrollPane1"); // NOI18N

        jScrollPane.setViewportView(jPanel);
        jScrollPane.setWheelScrollingEnabled(true);

        javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(networkFrame.getContentPane());
        networkFrame.getContentPane().setLayout(jInternalFrame1Layout);
        jInternalFrame1Layout.setHorizontalGroup(
                jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE));
        jInternalFrame1Layout.setVerticalGroup(
                jInternalFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE));

        networkFrame.putClientProperty(PROP_PANEL, jPanel);
        networkFrame.setVisible(true);
        jdpDesktop.add(networkFrame);
        try {
            networkFrame.setSelected(true);
        } catch (PropertyVetoException ex) {
            // Do nothing
        }
    }

    @Action
    public void exportParking() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr == null) {
            return;
        }
        EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
        if (panel == null) {
            return;
        }

        Airport airport = panel.getAirport();
        ReferencePoint p0 = airport.getRefPoint0();
        Double lat0 = p0.getLatitude();
        Double lon0 = p0.getLongitude();
        if (lat0 == null || lon0 == null) {
            JOptionPane.showMessageDialog(getFrame(), "No lat/lon set for first reference point", "Cannot export", JOptionPane.ERROR_MESSAGE);
            return;
        }
        ReferencePoint p1 = airport.getRefPoint1();
        Double lat1 = p1.getLatitude();
        Double lon1 = p1.getLongitude();
        if (lat1 == null || lon1 == null) {
            JOptionPane.showMessageDialog(getFrame(), "No lat/lon set for second reference point", "Cannot export", JOptionPane.ERROR_MESSAGE);
            return;
        }

        LinearInterpolator latitude = new LinearInterpolator(p0.getY(), lat0, p1.getY(), lat1);
        LinearInterpolator longitude = new LinearInterpolator(p0.getX(), lon0, p1.getX(), lon1);

        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select parking file to create");
        fc.setFileFilter(FILTER_XML);
        int result = fc.showSaveDialog(getFrame());
        if (result != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File f = fc.getSelectedFile();
        String filename = f.getAbsolutePath();
        if (f.exists()) {
            if (!f.canWrite()) {
                JOptionPane.showMessageDialog(this.getFrame(), filename + " is not writeable", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            int option = JOptionPane.showConfirmDialog(this.getFrame(), "Are you sure you want to overwrite " + filename + "?", "Confirm", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (option != JOptionPane.OK_OPTION) {
                return;
            }
        }

        String msg = null;
        FileOutputStream fos = null;
        Parking parking = new Parking(airport, latitude, longitude);
        try {
            fos = new FileOutputStream(f);
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(fos));
            parking.writeParking(pw);
            FileUtils.closeQuietly(pw);

        } catch (IOException ex) {
            msg = "Can't export to file named '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();

        } finally {
            FileUtils.closeQuietly(fos);
        }
        if (msg != null) {
            JOptionPane.showMessageDialog(getFrame(), msg, "Caught exception", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     *
     */
    @Action
    public void createGate() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr == null) {
            return;
        }
        EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
        if (panel == null) {
            return;
        }
        TerminalGate gate = new TerminalGate();
        gate.setGateType("Gate");
        gate.setGateSize(50);
        gate.setHeading(0.0);
        gate.setAirlineCodes(null);
        gate.setNodeName(null);
        panel.setEditMode(EditMode.GATE, gate);
    }

    @Action
    public void createTaxiway() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                TaxiPath taxiway = new TaxiPath();
                taxiway.setDirection(TaxiPath.BOTH);
                panel.setEditMode(EditMode.SEGMENT, taxiway);
            }
        }
    }

    @Action
    public void createNode() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                TaxiNode node = new TaxiNode();
                node.setNodeName("AI waypointnode");
                panel.setEditMode(EditMode.NODE, node);
            }
        }
    }

    @Action
    public void deleteSelected() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr != null) {
            EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
            if (panel != null) {
                panel.deleteSelected();
            }
        }
    }

    @Action
    public void importParking() {
        JInternalFrame fr = jdpDesktop.getSelectedFrame();
        if (fr == null) {
            return;
        }
        EditPanel panel = (EditPanel) fr.getClientProperty(PROP_PANEL);
        if (panel == null) {
            return;
        }

        Airport airport = panel.getAirport();
        ReferencePoint p0 = airport.getRefPoint0();
        Double lat0 = p0.getLatitude();
        Double lon0 = p0.getLongitude();
        if (lat0 == null || lon0 == null) {
            JOptionPane.showMessageDialog(getFrame(), "No lat/lon set for first reference point", "Cannot import", JOptionPane.ERROR_MESSAGE);
            return;
        }
        ReferencePoint p1 = airport.getRefPoint1();
        Double lat1 = p1.getLatitude();
        Double lon1 = p1.getLongitude();
        if (lat1 == null || lon1 == null) {
            JOptionPane.showMessageDialog(getFrame(), "No lat/lon set for second reference point", "Cannot import", JOptionPane.ERROR_MESSAGE);
            return;
        }

        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select parking file to import");
        fc.setFileFilter(FILTER_XML);
        int result = fc.showOpenDialog(getFrame());
        if (result != JFileChooser.APPROVE_OPTION) {
            return;
        }

        File f = fc.getSelectedFile();
        String filename = f.getAbsolutePath();
        if (!f.exists()) {
            JOptionPane.showMessageDialog(this.getFrame(), "There is no file named " + filename, "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (!f.canRead()) {
            JOptionPane.showMessageDialog(this.getFrame(), filename + " is not readable", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        String msg = null;
        LinearInterpolator latitude = new LinearInterpolator(lat0, p0.getY(), lat1, p1.getY());
        LinearInterpolator longitude = new LinearInterpolator(lon0, p0.getX(), lon1, p1.getX());
        Parking parking = new Parking(airport, latitude, longitude);
        try {
            parking.parseParking(f);

        } catch (IOException ex) {
            msg = "Can't parse file '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();

        } catch (SAXException ex) {
            msg = "Parsing exception while parsing '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();

        } catch (ParserConfigurationException ex) {
            msg = "Parser configuration exception while parsing '" + filename + "'\n" + ex.getClass().getName() + ": " + ex.getMessage();
        }
        if (msg != null) {
            JOptionPane.showMessageDialog(getFrame(), msg, "Caught exception", JOptionPane.ERROR_MESSAGE);
        }
    }

}

// ----- Revision History -----
// $Log: UIAirportEditor.java,v $
// Revision 1.3  2008-06-12 14:05:46  gregh
// Code clean-up
//
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
