/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/flightgear/tools/airporteditor/TerminalGate.java,v 1.3 2008-06-12 14:05:46 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.flightgear.tools.airporteditor;

import com.greghawkes.utils.StringUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.io.PrintWriter;
import javax.swing.JFrame;

/**
 * An airport terminal gate.
 * 
 * @author $Author: gregh $
 * @version $Id: TerminalGate.java,v 1.3 2008-06-12 14:05:46 gregh Exp $
 */
public class TerminalGate extends TaxiNode {
    public static final int DEFAULT_SIZE = 50;

    private static final int GATE_SIZE = 20;
    private static final GeneralPath GATE_SYMBOL = new GeneralPath();
    static {
        GATE_SYMBOL.moveTo(1.0, 1.0);
        GATE_SYMBOL.lineTo(0.0, 0.0);
        GATE_SYMBOL.lineTo(1.0, -1.0);
        GATE_SYMBOL.moveTo(-1.0, 0.0);
        GATE_SYMBOL.lineTo(2.0, 0.0);
    }

    private String gateType; // cargo, gate, ramp, ga, or mil
    private int gateRadius;
    private double heading;
    private String airlineCodes;
    private String number;  // unused, likely to be removed

    /**
     * Fetch the type of terminal gate.
     * @return the type of terminal gate.
     */
    public String getGateType() {
        if (gateType == null) {
            gateType = "Gate";
        }
        return gateType;
    }

    /**
     * Set the type of terminal gate.
     * @param type the type of terminal gate.
     */
    public void setGateType(String type) {
        this.gateType = type;
    }

    /**
     * Fetch the size of the terminal gate.
     * <p>This attribute corresponds to the FlightGear "radius" attribute.</p>
     * @return the size of the terminal gate.
     */
    public int getGateSize() {
        return gateRadius;
    }

    /**
     * Set the size of the terminal gate.
     * <p>This attribute corresponds to the FlightGear "radius" attribute.</p>
     * @param size the size of the terminal gate.
     */
    public void setGateSize(int size) {
        this.gateRadius = size;
    }

    /**
     * Fetch the gate's compass heading.
     * <p>This is the direction that aircraft are facing when parked at this gate.</p>
     * @return the gate's compass heading, in degrees.
     */
    public double getHeading() {
        return heading;
    }

    /**
     * Set the gate's compass heading.
     * <p>This is the direction that aircraft are facing when parked at this gate.</p>
     * @param heading the gate's compass heading, in degrees.
     */
    public void setHeading(double heading) {
        this.heading = heading;
    }

    /**
     * Fetch the gate's airline codes.
     * <p>The airline codes determine which airline's aircraft can use this gate.</p>
     * @return the gate's airline codes.
     */
    public String getAirlineCodes() {
        if (airlineCodes == null) {
            airlineCodes = StringUtils.EMPTY;
        }
        return airlineCodes;
    }

    /**
     * Set the gate's airline codes.
     * <p>The airline codes determine which airline's aircraft can use this gate.</p>
     * @param airlineCodes the gate's airline codes.
     */
    public void setAirlineCodes(String airlineCodes) {
        this.airlineCodes = StringUtils.trimToNull(airlineCodes);
    }

    /**
     * Fetch the gate number.
     * @return the gate number
     */
    public String getNumber() {
        if (number == null) {
            number = StringUtils.EMPTY;
        }
        return number;
    }

    /**
     * Set the gate number.
     * @param number the gate number.
     */
    public void setNumber(String number) {
        this.number = StringUtils.trimToNull(number);
    }

    @Override
    public void draw(Graphics2D g, AffineTransform xfmBackground, int imgWidth, int imgHeight, double scalingFactor) {
        g.setColor(getDrawingColour());
        double[] p = {
            getX() * imgWidth,
            getY() * imgHeight
        };
        xfmBackground.transform(p, 0, p, 0, 1);
        int tx = (int) p[0];
        int ty = (int) p[1];
        g.drawOval(tx - GATE_SIZE/2, ty - GATE_SIZE/2, GATE_SIZE, GATE_SIZE);
        
        AffineTransform xfm = new AffineTransform(xfmBackground);
        xfm.translate(getX() * imgWidth, getY() * imgHeight);
        xfm.scale(GATE_SIZE/scalingFactor/2.0, GATE_SIZE/scalingFactor/2.0);
        xfm.rotate((heading + 90.0) * 2.0 * Math.PI / 360.0);

        GeneralPath t = new GeneralPath(GATE_SYMBOL);
        t.transform(xfm);
        
        g.setColor(Color.BLACK);
        g.draw(t);
        
        String name = getNodeName();
        if (name != null && name.length() > 0) {
            g.drawString(name, tx, ty);
        }
    }

    @Override
    public void println(PrintWriter pw) {
        pw.println("GATE\t" + getIdent() + "\t" + getX() + "\t" + getY() + "\t" + getNodeName() + "\t" + getGateType() + "\t" + getAirlineCodes() + "\t" + getGateSize() + "\t" + getHeading());
    }

    @Override
    public boolean editProperties(JFrame parent) {
        UIGateProperties gateBox = new UIGateProperties(parent, true);
        gateBox.setLocationRelativeTo(parent);
        gateBox.setType(getGateType());
        gateBox.setGateSize(getGateSize());
        gateBox.setHeading((int) getHeading());
        gateBox.setAirlineCodes(getAirlineCodes());
        gateBox.setGateName(getNodeName());
        gateBox.setVisible(true);

        if (gateBox.isAccepted()) {
            setGateType(gateBox.getType());
            setGateSize(gateBox.getGateSize());
            setHeading(gateBox.getHeading());
            setAirlineCodes(gateBox.getAirlineCodes());
            setNodeName(gateBox.getGateName());
            return true;

        } else {
            return false;
        }
    }

}

// ----- Revision History -----
// $Log: TerminalGate.java,v $
// Revision 1.3  2008-06-12 14:05:46  gregh
// Code clean-up
//
// Revision 1.2  2008-06-07 06:30:30  gregh
// Code clean-up, JavaDoc comments
//
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
