/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/FileUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;

/**
 * Various utility routines to handle files.
 * 
 * @author $Author: gregh $
 * @version $Id: FileUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class FileUtils {

    private static final String COMMENT_PREFIX = "#";
    private static final String DIR_SEPARATOR = System.getProperty("file.separator");
    private static final String CHAR_EXTENSION = ".";

    private FileUtils() {
    }

    /**
     * Close a closeable file, ignoring any errors.
     * @param c the <code>Closeable</code> to be closed, may be <code>null</code>.
     */
    public static final void closeQuietly(Closeable c) {
        if (c != null) {
            try {
                c.close();

            } catch (IOException iox) {
                // Do nothing
            }
        }
    }

    /**
     * Read the next line of text, skipping comments and empty lines.
     * @param br the <code>BufferedReader</code> from which to read the line.
     * @return the next line, or <code>null</code> if at the end of the file.
     * @throws java.io.IOException if the file cannot be read.
     */
    public static final String readLine(BufferedReader br) throws IOException {
        for ( ; ; ) {
            String s = br.readLine();
            if (s == null) {
                return null;

            } else if (s.startsWith(COMMENT_PREFIX)) {
                continue;

            } else {
                s = StringUtils.trimToNull(s);
                if (s != null) {
                    return s;
                }
            }
        }
    }

    /**
     * Changes the filename's extension to the specified value.
     * <p>If the filename already has the correct extension, it is not changed.</p>
     * @param filename the filename to be modified.
     * @param extension the file extension to be applied to the file name.
     * @return the filename with its extension changed to the specified value.
     */
    public static final String setExtension(String filename, String extension) {
        // Find the start of the filename, after the last directory name
        int pos0 = filename.lastIndexOf(DIR_SEPARATOR) + 1;
        
        // Find the startof the extension, after the last "dot" character
        int pos1 = filename.lastIndexOf(CHAR_EXTENSION);
        if (pos1 < pos0) {
            // filename has no extension; add the one we want
            return filename + CHAR_EXTENSION + extension;

        } else if (filename.substring(pos1 + 1).equalsIgnoreCase(extension)) {
            return filename;

        } else {
            // Replace existing extension with the one we want
            return filename.substring(0, pos1 + 1) + extension;
        }
    }

}

// ----- Revision History -----
// $Log: FileUtils.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
