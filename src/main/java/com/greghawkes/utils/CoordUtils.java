/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/CoordUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.StringTokenizer;

/**
 * Utilities for working with latitudes and longitudes.
 * 
 * @author gregh
 */
public class CoordUtils {
    private static final double MINS_PER_DEG = 60.0;
    private static final double SECS_PER_MIN = 60.0;
    private static final NumberFormat FMT_DEGREES = new DecimalFormat();
    private static final NumberFormat FMT_MINUTES = new DecimalFormat();
    private static final NumberFormat FMT_SECONDS = new DecimalFormat();
    static {
        FMT_DEGREES.setMinimumFractionDigits(6);
        FMT_MINUTES.setMinimumFractionDigits(4);
        FMT_SECONDS.setMinimumFractionDigits(2);
    }

    private CoordUtils() {
    }

    /**
     * Test whether the value is a valid latitude.
     * <p>Valid latitudes lie between -90 and +90 degrees.</p>
     * @param angle the angle to be tested, in degrees.
     * @return True if the angle is a valid latitude; False, otherwise.
     */
    public static boolean isValidLatitude(double angle) {
        return (-90.0 <= angle) && (angle <= 90.0);
    }

    /**
     * Test whether the value is a valid longitude.
     * <p>Valid longitudes lie between -180 and +180 degrees.</p>
     * @param angle the angle to be tested, in degrees.
     * @return True if the angle is a valid longitude; False, otherwise.
     */
    public static boolean isValidLongitude(double angle) {
        return (-180.0 <= angle) && (angle <= 180.0);
    }

    /**
     * Format an angle as a string of the form &quot;[-]ddd.dddddd&quot;.
     * @param angle the angle to be formatted, in degrees.
     * @return a string of the form &quot;[-]ddd.dddddd&quot;.
     */
    public static String format(double angle) {
        return FMT_DEGREES.format(angle);
    }

    /**
     * Format an angle as a string of the form &quot;[N | S] ddd mm.mmmm&quot;.
     * @param angle the angle to be formatted, in degrees.
     * @return a string of the form &quot;[N | S] ddd mm.mmmm&quot;.
     */
    public static String formatLatDegMin(double angle) {
        if (angle < 0.0) {
            return "S" + formatDegMin(-angle);
        } else {
            return "N" + formatDegMin(angle);
        }
    }

    /**
     * Format an angle as a string of the form &quot;[E | W] ddd mm.mmmm&quot;.
     * @param angle the angle to be formatted, in degrees.
     * @return a string of the form &quot;[E | W] ddd mm.mmmm&quot;.
     */
    public static String formatLonDegMin(double angle) {
        if (angle < 0.0) {
            return "W" + formatDegMin(-angle);
        } else {
            return "E" + formatDegMin(angle);
        }
    }

    /**
     * Attempt to parse a string as a lat/lon value.
     * <p>Will parse a string of the form [ N | E | S | W | - ] &lt;deg&gt; [&lt;min&gt;] [&lt;sec&gt;]</p>
     * @param s the string to parse.
     * @return the angle corresponding to the string.
     */
    public static double parse(String s) {
        String t = StringUtils.trimToNull(s);
        if (t == null) {
            throw new IllegalArgumentException("Can't parse empty value");
        }
        
        int sign = 0;
        char c = t.toLowerCase().charAt(0);
        if (Character.isLetter(c)) {
            // Test for prefix 'N', 'S', 'E', or 'W'
            if (c == 'n' || c == 'e') {
                // Northern or eastern hemisphere
                sign = 1;
                t = t.substring(1);

            } else if (c == 's' || c == 'w') {
                // Southern or western hemisphere; change sign
                sign = -1;
                t = t.substring(1);

            } else {
                // Some other character
                throw new IllegalArgumentException("Hemisphere prefix is not valid");
            }
        }

        StringTokenizer st = new StringTokenizer(t);
        if (st.countTokens() == 0) {
            throw new IllegalArgumentException("Must contain degrees value");
        }

        Double deg = NumberUtils.parseDouble(st.nextToken());
        if (deg == null) {
            throw new IllegalArgumentException("Can't parse degrees value");
        }
        if (deg < 0.0 && sign != 0) {
            throw new IllegalArgumentException("Degrees must be positive value when prefixed with hemisphere");
        }
        if (deg < 0.0) {
            sign = -1;
            deg = -deg;
        }
        if (sign == 0) {
            sign = 1;
        }

        Double min = null;
        if (st.countTokens() > 0) {
            min = NumberUtils.parseDouble(st.nextToken());
            if (min == null) {
                throw new IllegalArgumentException("Can't parse minutes value");
            }
            if (min < 0.0) {
                throw new IllegalArgumentException("Minutes must be positive value");
            }
        } else {
            min = 0.0;
        }

        Double sec = null;
        if (st.countTokens() > 0) {
            sec = NumberUtils.parseDouble(st.nextToken());
            if (sec == null) {
                throw new IllegalArgumentException("Can't parse seconds value");
            }
            if (sec < 0.0) {
                throw new IllegalArgumentException("Seconds must be positive value");
            }
        } else {
            sec = 0.0;
        }

        if (st.countTokens() > 0) {
            throw new IllegalArgumentException("Too many components");
        }

        return sign * (deg + (min + (sec / SECS_PER_MIN)) / MINS_PER_DEG);
    }
    
    private static String formatDegMin(double val) {
        int deg = (int) Math.floor(val);
        double min = (val - deg) * MINS_PER_DEG;

        return deg + " " + FMT_MINUTES.format(min);
    }
    
}

// ----- Revision History -----
// $Log: CoordUtils.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
