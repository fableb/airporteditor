/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/NumberUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

/**
 * Various utility routines to parse numeric values.
 * 
 * @author $Author: gregh $
 * @version $Id: NumberUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class NumberUtils {

    private NumberUtils() {
    }

    /**
     * Parse a string as a Double value.
     * @param s the string to be parsed.
     * @param defaultValue the default value to be returned if the string cannot be parsed as a Double.
     * @return the Double value of the String, or <code>defaultValue</code> if the string cannot be parsed as a Double.
     */
    public static final Double parseDouble(String s, Double defaultValue) {
        try {
            return Double.valueOf(s);

        } catch (NumberFormatException nfx) {
            return defaultValue;
        }
    }

    /**
     * Parse a string as a Double value.
     * @param s the string to be parsed.
     * @return the Double value of the string, or <code>null</code> if the string cannot be parsed as a Double.
     */
    public static final Double parseDouble(String s) {
        return parseDouble(s, null);
    }

    /**
     * Parse a string as an Integer value.
     * @param s the string to be parsed.
     * @param defaultValue the default value to return if the string cannot be parsed as an Integer.
     * @return the Integer value of the String, or <code>defaultValue</code> if the string cannot be parsed as an Integer.
     */
    public static final Integer parseInteger(String s, Integer defaultValue) {
        try {
            return Integer.valueOf(s);

        } catch (NumberFormatException nfx) {
            return defaultValue;
        }
    }

    /**
     * Parse a string as an Integer value.
     * @param s the string to be parsed.
     * @return the Integer value of the String, or <code>null</code> if the string cannot be parsed as an Integer.
     */
    public static final Integer parseInteger(String s) {
        return parseInteger(s, null);
    }

}

// ----- Revision History -----
// $Log: NumberUtils.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
