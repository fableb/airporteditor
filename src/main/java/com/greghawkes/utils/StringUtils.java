/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/StringUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

/**
 * Various utility routes to handle Strings.
 * 
 * @author $Author: gregh $
 * @version $Id: StringUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class StringUtils {

    private StringUtils() {
    }

    /**
     * The empty string.
     */
    public static final String EMPTY = "";

    /**
     * Remove leading and trailing whitespace from a string.
     * <p>Handes a <code>Null</code> input string gracefully;
     * will return <code>Null</code> if the result is empty after trimming.</p>
     * @param s the string value to be trimmed, may be null.
     * @return the string value without leading and trailing spaces.
     */
    public static final String trimToNull(String s) {
        if (s == null) {
            return null;
        }
        String t = s.trim();
        return (t.length() > 0 ? t : null);
    }

    /**
     * Convert any XML-significant characters in the string to their XML-escaped equivalents.
     * @param s the string.
     * @return the XML-escaped string.
     */
    public static final String escapeXML(String s) {
        if (s == null) {
            return null;
        }

        StringBuffer buf = new StringBuffer((s.length() * 120) / 100 + 5);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '"') {
                buf.append("&#34;");
            } else if (c == '\'') {
                buf.append("&#39;");
            } else if (c == '<') {
                buf.append("&#60;");
            } else if (c == '>') {
                buf.append("&#62;");
            } else if (c == '&') {
                buf.append("&#38;");
            } else {
                buf.append(c);
            }
        }
        return buf.toString();
    }

}

// ----- Revision History -----
// $Log: StringUtils.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
