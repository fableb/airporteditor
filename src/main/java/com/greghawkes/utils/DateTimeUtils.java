/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/DateTimeUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Various utility routines for date/time handling.
 *
 * @author $Author: gregh $
 * @version $Id: DateTimeUtils.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class DateTimeUtils {
    private static final DateFormat FMT_ISO_DATETIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    private static final DateFormat FMT_DATETIME = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

    /**
     * Private constructor.
     */
    private DateTimeUtils() {
    }

    /**
     * Format the specified date/time into the form &quot;dd-MMM-yyyy HH:mm:ss&quot;.
     * @return a string in the form &quot;dd-MMM-yyyy HH:mm:ss&quot;.
     */
    public static final String getDateTimeLabel(Date d) {
        synchronized (FMT_DATETIME) {
            return FMT_DATETIME.format(d);
        }
    }

    /**
     * Return the specified date/time in the ISO standard form &quot;yyyy-MM-dd'T'HH:mm:ss&quot;.
     * @return a string in the ISO standard form &quot;yyyy-MM-dd'T'HH:mm:ss&quot;.
     */
    public static final String isoDateTime(Date d) {
        synchronized (FMT_ISO_DATETIME) {
            return FMT_ISO_DATETIME.format(d);
        }
    }

}

// ----- Revision History -----
// $Log: DateTimeUtils.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
