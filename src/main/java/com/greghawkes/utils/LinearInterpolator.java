/*
 * $Header: /var/lib/cvs/AirportEditor/src/com/greghawkes/utils/LinearInterpolator.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 * Copyright 2008 Gregory D. Hawkes.
 *
 * This file is part of AirportEditor.
 *
 * AirportEditor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirportEditor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirportEditor.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.greghawkes.utils;

/**
 * Interpolates values along a line.
 * 
 * @author $Author: gregh $
 * @version $Id: LinearInterpolator.java,v 1.1.1.1 2008-06-04 13:59:36 gregh Exp $
 */
public class LinearInterpolator {

    private double x0;
    private double y0;
    private double slope;
    
    /**
     * Constructs the LinearInterpolator, given the two points (x0,y0) and (x1,y1).
     * @param x0 first X-ordinate.
     * @param y0 first Y-ordinate.
     * @param x1 second X-ordinate.
     * @param y1 second Y-ordinate.
     */
    public LinearInterpolator(double x0, double y0, double x1, double y1) {
        this.x0 = x0;
        this.y0 = y0;
        slope = (y1 - y0) / (x1 - x0);
    }

    /**
     * Calculate the value of the Y-ordinate at the specified X ordinate.
     * @param x the value of the X-ordindate.
     * @return the value of the Y-ordinate.
     */
    public double valueAt(double x) {
        double result = y0 + (x - x0) * slope;
        return result;
    }

}

// ----- Revision History -----
// $Log: LinearInterpolator.java,v $
// Revision 1.1.1.1  2008-06-04 13:59:36  gregh
// File imported into CVS repository
//
